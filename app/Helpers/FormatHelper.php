<?php

namespace App\Helpers;

use ChrisUllyott\FileSize;
use Illuminate\Support\Facades\App;
use NumberFormatter;

class FormatHelper
{
    public static function filesize(int $bytes): string
    {
        $s = new FileSize($bytes, 10);

        return $s;
    }

    public static function number(int $number): string
    {
        $f = NumberFormatter::create(App::getLocale(), NumberFormatter::DECIMAL);

        return $f->format($number);
    }
}
