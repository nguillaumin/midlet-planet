<?php

namespace App\Helpers;

class MIDletHelper
{
    public static function mimeTypeIcon(string $mimeType): string
    {
        $type = explode('/', $mimeType)[0];
        switch ($type) {
            case 'image':
                return 'bi-file-earmark-image';
            case 'audio':
                return 'bi-file-earmark-music';
            case 'text':
                return 'bi-file-earmark-text';
            case 'font':
                return 'bi-file-earmark-font';
            case 'video':
                return 'bi-file-earmark-play';
            default:
                return 'bi-file-earmark';
        }
    }
}
