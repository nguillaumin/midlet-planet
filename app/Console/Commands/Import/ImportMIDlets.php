<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class ImportMIDlets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'midlet:import {path?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import midlets';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $importer = new MIDletImporter($this);

        $path = $this->argument('path') ?? Storage::path('import');
        if (is_dir($path)) {
            $count = 0;

            $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
            foreach ($iterator as $file) {
                if ($file->isDir() || $file->isLink()) {
                    continue;
                }

                $this->line("Processing {$file}");
                $importer->importMIDlet($file);
                $count++;
            }

            $this->info("Processed {$count} files");
        } else {
            $importer->importMIDlet($path);
        }
    }
}
