<?php

namespace App\Console\Commands\Import;

use Illuminate\Support\Str;

class JarManifest
{
    const KEY_MIDLET_NAME = 'MIDlet-Name';

    const KEY_MIDLET_VENDOR = 'MIDlet-Vendor';

    const KEY_MIDLET_VERSION = 'MIDlet-Version';

    private array $manifest;

    public function __construct(string $manifest)
    {
        $this->manifest = JarManifest::parseManifest($manifest);
    }

    private static function parseManifest(string $manifest): array
    {
        return collect(explode("\n", $manifest))
            ->map(function ($line) {
                // Force utf-8 conversion to remove invalid characters, as some
                // manifests are incorrectly non-utf-8 encoded
                $converted = mb_convert_encoding($line, 'utf-8', 'utf-8');
                preg_match('/^([^:]+?):(.*)$/', $converted, $matches);
                if (count($matches) === 3) {
                    return [
                        'key'   => trim($matches[1]),
                        'value' => trim($matches[2]),
                    ];
                } else {
                    return null;
                }
            })
            ->filter(fn ($attr) => $attr !== null)
            ->groupBy('key')
            ->flatMap(function ($attr, $key) {
                return [
                    $key => collect($attr)
                        ->map(fn ($attr) => $attr['value'])
                        ->toArray(),
                ];
            })
            ->toArray();
    }

    public function getMIDletName(): ?string
    {
        return $this->getSingleValue(JarManifest::KEY_MIDLET_NAME);
    }

    public function getMIDletVendor(): ?string
    {
        return $this->getSingleValue(JarManifest::KEY_MIDLET_VENDOR);
    }

    public function getMIDletVersion(): ?string
    {
        return $this->getSingleValue(JarManifest::KEY_MIDLET_VERSION);
    }

    public function getMIDletCount(): int
    {
        $max = collect(array_keys($this->manifest))
            ->filter(fn ($key) => Str::of($key)->test('/^MIDlet-\d+$/'))
            ->count();

        return $max ?? 0;
    }

    public function getMIDlet(int $index): array
    {
        $value = $this->getSingleValue("MIDlet-{$index}");
        if ($value) {
            $data = collect(explode(',', $value))
                ->map(fn ($s) => trim($s))
                ->toArray();

            return [
                'name'  => $data[0],
                'icon'  => $data[1] ?? '',
                'class' => $data[2] ?? '',
            ];
        } else {
            return [];
        }
    }

    public function getSingleValue(string $key): ?string
    {
        return in_array($key, array_keys($this->manifest))
            ? $this->manifest[$key][0]
            : null;
    }

    public function __get(string $name)
    {
        return $this->$name ?? null;
    }
}
