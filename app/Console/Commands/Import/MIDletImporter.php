<?php

namespace App\Console\Commands\Import;

use App\Models\MIDlet;
use App\Models\MIDletAsset;
use App\Models\MIDletManifestAttributes;
use App\Models\MIDletVendor;
use Error;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ZipArchive;

class MIDletImporter
{
    const TITLE_CANDIDATES = [
        'title', 'splash', 'intro', 'start', 'cover',
        'introscreen', 'logo', 'banner',
    ];

    private Command $cmd;

    public function __construct(Command $cmd)
    {
        $this->cmd = $cmd;
    }

    public function importMIDlet(string $file)
    {
        if (pathinfo($file, PATHINFO_EXTENSION) !== 'jar') {
            return;
        }

        $hash = hash_file('sha256', $file);
        $existing = MIDlet::firstWhere('sha256', $hash);
        if ($existing !== null) {
            $this->cmd->info('Found existing MIDlet ' . $existing->name . ' (' . $existing->getKey() . ") for {$file}.");

            return;
        }

        $zip = new ZipArchive();
        if ($zip->open($file, ZipArchive::RDONLY) !== true) {
            $this->cmd->error("Error opening {$file}");

            return;
        }

        $screen_width = null;
        $screen_height = null;
        preg_match('/(\d\d\d)[×x](\d\d\d)/', $file, $screenSize);
        if (count($screenSize) === 3) {
            $screen_width = $screenSize[1];
            $screen_height = $screenSize[2];
        }

        // Lookup manifest index case-insensitive. Some MIDlets are wrongly
        // storing it under a lower case 'meta-inf/' directory
        $manifest = null;
        $manifestIndex = $zip->locateName('META-INF/MANIFEST.MF', ZipArchive::FL_NOCASE);
        if ($manifestIndex !== false) {
            $manifest = new JarManifest($zip->getFromIndex($manifestIndex));
        }

        $midlet = MIDlet::create([
            'filename'      => basename($file),
            'filesize'      => filesize($file),
            'name'          => $manifest?->getMIDletName() ?: basename($file),
            'version'       => $manifest?->getMIDletVersion(),
            'screen_width'  => $screen_width,
            'screen_height' => $screen_height,
            'count'         => $manifest?->getMIDletCount() ?? 0,
            'sha256'        => $hash,
            'sha1'          => hash_file('sha1', $file),
            'crc32'         => hash_file('crc32', $file),
        ]);

        if ($manifest?->getMIDletVendor()) {
            $vendor = MIDletVendor::firstOrCreate([
                'name' => $manifest->getMIDletVendor(),
            ]);
            $vendor->midlets()->save($midlet);
        }

        collect($manifest?->manifest ?? [])
            ->flatMap(function ($values, $key) {
                return collect($values)
                    ->map(function ($value) use ($key) {
                        return new MIDletManifestAttributes([
                            'key'    => $key,
                            'value'  => $value,
                        ]);
                    });
            })
            ->each(function ($attr) use ($midlet) {
                $midlet->manifestAttributes()->save($attr);
            });

        $title = $this->findTitle($zip);
        if ($title) {
            $ext = pathinfo($title, PATHINFO_EXTENSION);
            $asset = new MIDletAsset([
                'filename'  => $title,
                'ext'       => $ext,
                'mime_type' => "image/{$ext}",
                'type'      => 'title',
            ]);
            $asset->midlet_id = $midlet->getKey();
            $asset->save();

            Storage::put($asset->path, $zip->getFromName($title));
        }

        $icon = $this->findIcon($manifest);
        if ($icon && Str::of($icon)->startsWith('/')) {
            $icon = substr($icon, 1);
        }

        if ($icon && $zip->locateName($icon)) {
            $ext = pathinfo($icon, PATHINFO_EXTENSION);
            $asset = new MIDletAsset([
                'filename'  => $icon,
                'ext'       => $ext,
                'mime_type' => "image/{$ext}",
                'type'      => 'icon',
            ]);
            $asset->midlet_id = $midlet->getKey();
            $asset->save();

            Storage::put($asset->path, $zip->getFromName($icon));

            // Some icon files don't have an extension, try to handle this case
            if (! $asset->ext) {
                $oldPath = $asset->path;
                $ext = $this->findIconFormat(Storage::path($asset->path));
                if ($ext) {
                    $asset->update([
                        'ext'       => $ext,
                        'mime_type' => "image/{$ext}",
                    ]);

                    Storage::move($oldPath, $asset->path);
                }
            }
        }

        $exclude = ['META-INF/MANIFEST.MF', $title, $icon];
        $this->findMedia($midlet, $zip, $exclude);

        $zip->close();
    }

    private function findTitle(ZipArchive $zip): ?string
    {
        $files = [];
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $stat = $zip->statIndex($i);
            $files[] = $stat['name'];
        }

        $file = collect($files)
            ->first(function ($file) {
                $found = collect(MIDletImporter::TITLE_CANDIDATES)
                    ->first(function ($candidate) use ($file) {
                        return Str::of(basename($file))->test("/{$candidate}.*\.(gif|png|jpg|jpeg)/");
                    });

                return $found != null;
            });

        return $file;
    }

    private function findIcon(?JarManifest $manifest): ?string
    {
        $midlet = $manifest?->getMIDlet(1);
        if ($midlet !== null && in_array('icon', array_keys($midlet)) && $midlet['icon'] !== '') {
            return $midlet['icon'];
        } else {
            return $manifest?->getSingleValue('MIDlet-Icon');
        }
    }

    private function findIconFormat(string $icon): ?string
    {
        $ext = pathinfo($icon, PATHINFO_EXTENSION);
        if ($ext) {
            return $ext;
        }

        $contentType = mime_content_type($icon);
        if ($contentType && str_starts_with($contentType, 'image/')) {
            return explode('/', $contentType)[1];
        }

        return null;
    }

    private function findMedia(MIDlet $midlet, ZipArchive $zip, array $exclude)
    {
        $excludeLowerCase = collect($exclude)
            ->map(fn ($item) => strtolower($item))
            ->toArray();

        for ($i = 0; $i < $zip->numFiles; $i++) {
            $filename = $zip->getNameIndex($i);
            if (in_array(strtolower($filename), $excludeLowerCase)) {
                continue;
            }
            if (str_ends_with($filename, '.class')) {
                continue;
            }

            $tmp = tempnam(sys_get_temp_dir(), MIDletImporter::class);
            file_put_contents($tmp, $zip->getFromIndex($i));
            $mimeType = mime_content_type($tmp);

            if (str_starts_with($mimeType, 'application/') || in_array($mimeType, [
                // Add other types to exclude here
            ])) {
                continue;
            }

            if (Str::of($mimeType)->test('/^(image|audio|text|video|font|message|x-epoc)\//')) {
                $asset = new MIDletAsset([
                    'filename'  => $filename,
                    'ext'       => explode('/', $mimeType)[1],
                    'mime_type' => $mimeType,
                    'type'      => 'asset',
                ]);
                $asset->midlet_id = $midlet->getKey();
                $asset->save();

                Storage::put($asset->path, file_get_contents($tmp));
            } else {
                throw new Error("Unhandled mime type '{$mimeType}' for {$filename}");
                $this->cmd->info("Mime type for {$filename}: {$mimeType}");
            }

            unlink($tmp);
        }
    }
}
