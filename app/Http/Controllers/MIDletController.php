<?php

namespace App\Http\Controllers;

use App\Models\MIDlet;
use App\Models\MIDletAsset;
use App\Models\MIDletVendor;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MIDletController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'vendor' => 'nullable|numeric',
        ]);

        $selectedVendor = null;
        $midlets = MIDlet::orderBy('name');
        if ($request->vendor) {
            $selectedVendor = MIDletVendor::findOrFail($request->vendor);
            $midlets = $midlets->whereHas('vendor', function (Builder $query) use ($selectedVendor) {
                $query->where('id', '=', $selectedVendor->getKey());
            });
        }

        $midlets = $midlets
            ->paginate(20)
            ->withQueryString();

        $vendors = DB::table('midlet_vendors')
            ->selectRaw('midlet_vendors.id, midlet_vendors.name, count(midlets.id) as midlets_count')
            ->join('midlets', 'midlet_vendors.id', 'midlets.midlet_vendor_id')
            ->groupBy('midlet_vendors.id', 'midlet_vendors.name')
            ->orderByRaw('midlets_count desc')
            ->get();

        return view('midlets.index', [
            'midlets'         => $midlets,
            'vendors'         => $vendors,
            'selectedVendor'  => $selectedVendor,
        ]);
    }

    public function show(MIDlet $midlet)
    {
        return view('midlets.show', [
            'midlet' => $midlet,
        ]);
    }

    public function title(MIDlet $midlet)
    {
        return response(Storage::get($midlet->title->path), 200, [
            'Content-Type' => $midlet->title->mime_type,
        ]);
    }

    public function icon(MIDlet $midlet)
    {
        return response(Storage::get($midlet->icon->path), 200, [
            'Content-Type' => $midlet->icon->mime_type,
        ]);
    }

    public function asset(MIDlet $midlet, MIDletAsset $asset)
    {
        return response(Storage::get($asset->path), 200, [
            'Content-Type'        => $asset->mime_type,
            'Content-Disposition' => 'inline; filename="' . $asset->filename . '"',
        ]);
    }
}
