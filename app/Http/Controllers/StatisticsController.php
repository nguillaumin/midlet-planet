<?php

namespace App\Http\Controllers;

use App\Models\MIDlet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    public function index(Request $request)
    {
        $midletCount = DB::table('midlets')->count();
        $totalSize = DB::table('midlets')->sum('filesize');
        $titleCount = DB::table('midlet_assets')
            ->where('type', '=', 'title')
            ->count();
        $iconCount = DB::table('midlet_assets')
            ->where('type', '=', 'icon')
            ->count();
        $assetCount = DB::table('midlet_assets')->count();
        $distinctManifestKeys = DB::table('midlet_manifest_attributes')
            ->distinct('key')
            ->count('key');
        $topManifestAttributes = DB::table('midlet_manifest_attributes')
            ->select('key')
            ->selectRaw('count(midlets.id) as midlets_count')
            ->join('midlets', 'midlet_manifest_attributes.midlet_id', 'midlets.id')
            ->groupBy('key')
            ->orderByRaw('midlets_count desc')
            ->limit(10)
            ->get();
        $topAssetMimeTypes = DB::table('midlet_assets')
            ->select('mime_type')
            ->selectRaw('count(midlet_assets.id) as asset_count')
            ->groupBy('mime_type')
            ->orderByRaw('asset_count desc')
            ->limit(10)
            ->get();

        $biggestMIDlet = MIDlet::orderByDesc('filesize')
            ->limit(1)
            ->first();

        $smallestMIDlet = MIDlet::orderBy('filesize')
            ->limit(1)
            ->first();

        return view('statistics.index', [
            'midletCount'           => $midletCount,
            'totalSize'             => $totalSize,
            'titleCount'            => $titleCount,
            'iconCount'             => $iconCount,
            'assetCount'            => $assetCount,
            'distinctManifestKeys'  => $distinctManifestKeys,
            'topManifestAttributes' => $topManifestAttributes,
            'biggestMIDlet'         => $biggestMIDlet,
            'smallestMIDlet'        => $smallestMIDlet,
            'topAssetMimeTypes'     => $topAssetMimeTypes,
        ]);
    }
}
