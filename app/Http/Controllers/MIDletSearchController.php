<?php

namespace App\Http\Controllers;

use App\Models\MIDlet;
use App\Models\MIDletVendor;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class MIDletSearchController extends Controller
{
    public function index()
    {
        $vendors = MIDletVendor::orderBy('name')
            ->get();
        $assetTypes = DB::table('midlet_assets')
            ->select('mime_type')
            ->groupBy('mime_type')
            ->get()
            ->pluck('mime_type')
            ->sort();
        $screenWidths = DB::table('midlets')
            ->select('screen_width')
            ->whereNotNull('screen_width')
            ->distinct('screen_width')
            ->get()
            ->pluck('screen_width');
        $screenHeights = DB::table('midlets')
            ->select('screen_height')
            ->whereNotNull('screen_height')
            ->distinct('screen_height')
            ->get()
            ->pluck('screen_height');

        return view('search.search', [
            'vendors'       => $vendors,
            'assetTypes'    => $assetTypes,
            'screenWidths'  => $screenWidths,
            'screenHeights' => $screenHeights,
        ]);
    }

    public function search(Request $request)
    {
        // dd($request->title);

        $request->validate([
            'vendor'        => 'nullable|numeric',
            'title'         => ['nullable', Rule::in(['true', 'false'])],
            'midlets'       => 'nullable|numeric',
            'screen_width'  => 'nullable|numeric',
            'screen_height' => 'nullable|numeric',
        ]);
        $midlets = MIDlet::orderBy('name');

        if ($request->name) {
            $midlets = $midlets->where(DB::raw('lower(name)'), 'like', '%' . strtolower($request->name) . '%');
        }

        if ($request->filename) {
            $midlets = $midlets->where(DB::raw('lower(filename)'), 'like', '%' . strtolower($request->filename) . '%');
        }

        if ($request->vendor) {
            $midlets = $midlets->whereHas('vendor', function (Builder $query) use ($request) {
                $query->where('id', $request->vendor);
            });
        }

        if ($request->title !== null) {
            if ($request->title === 'true') {
                $midlets = $midlets->whereHas('assets', function (Builder $query) {
                    $query->where('type', '=', 'title');
                });
            } else {
                $midlets = $midlets->whereDoesntHave('assets', function (Builder $query) {
                    $query->where('type', '=', 'title');
                });
            }
        }

        if ($request->icon !== null) {
            if ($request->icon === 'true') {
                $midlets = $midlets->whereHas('assets', function (Builder $query) {
                    $query->where('type', '=', 'icon');
                });
            } else {
                $midlets = $midlets->whereDoesntHave('assets', function (Builder $query) {
                    $query->where('type', '=', 'icon');
                });
            }
        }

        if ($request->midlets !== null) {
            if ($request->midlets >= 3) {
                $midlets = $midlets->where('count', '>=', 3);
            } else {
                $midlets = $midlets->where('count', '=', $request->midlets);
            }
        }

        if ($request->assetType !== null) {
            $midlets = $midlets->whereHas('assets', function (Builder $query) use ($request) {
                $query->where('mime_type', '=', $request->assetType);
            });
        }

        if ($request->screenWidth) {
            $midlets = $midlets->where('screen_width', '=', $request->screenWidth);
        }
        if ($request->screenHeight) {
            $midlets = $midlets->where('screen_height', '=', $request->screenHeight);
        }

        $midlets = $midlets->paginate(20)
            ->withQueryString();

        return view('search.results', [
            'midlets' => $midlets,
        ]);
    }
}
