<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MIDlet extends Model
{
    use HasFactory;

    protected $table = 'midlets';

    protected $fillable = [
        'filename', 'filesize', 'name', 'version', 'screen_width', 'screen_height',
        'count', 'sha256', 'sha1', 'crc32',
    ];

    public function manifestAttributes()
    {
        return $this->hasMany(MIDletManifestAttributes::class, 'midlet_id');
    }

    public function assets()
    {
        return $this->hasMany(MIDletAsset::class, 'midlet_id');
    }

    public function getTitleAttribute()
    {
        return $this->assets
            ->first(fn ($asset) => $asset->type === 'title');
    }

    public function getIconAttribute()
    {
        return $this->assets
            ->first(fn ($asset) => $asset->type === 'icon');
    }

    public function vendor()
    {
        return $this->belongsTo(MIDletVendor::class, 'midlet_vendor_id');
    }

    public function getManifestAttribute(string $key)
    {
        return MIDletManifestAttributes::where('midlet_id', '=', $this->getKey())
            ->where('key', '=', $key)
            ->first()
            ?->value;
    }
}
