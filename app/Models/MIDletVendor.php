<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MIDletVendor extends Model
{
    use HasFactory;

    protected $table = 'midlet_vendors';

    protected $fillable = ['name'];

    public function midlets()
    {
        return $this->hasMany(MIDlet::class, 'midlet_vendor_id');
    }
}
