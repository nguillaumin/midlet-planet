<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class MIDletAsset extends Model
{
    use HasFactory;

    const CLUSTER_SIZE = 10000;

    protected $table = 'midlet_assets';

    protected $fillable = ['ext', 'filename', 'type', 'mime_type'];

    protected static function booted(): void
    {
        static::deleted(function (MIDletAsset $asset) {
            Storage::delete($asset->path);
        });
    }

    public function getPathAttribute()
    {
        return 'assets/' . $this->getClusterFolder() . '/' .
            $this->getKey() . '.' . $this->ext;
    }

    public function getMainTypeAttribute()
    {
        return explode('/', $this->mime_type)[0];
    }

    public function getSubtypeAttribute()
    {
        return explode('/', $this->mime_type)[1];
    }

    private function getClusterFolder()
    {
        return floor($this->getKey() / MIDletAsset::CLUSTER_SIZE);
    }
}
