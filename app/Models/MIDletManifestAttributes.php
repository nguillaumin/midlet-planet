<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MIDletManifestAttributes extends Model
{
    use HasFactory;

    protected $table = 'midlet_manifest_attributes';

    protected $fillable = ['key', 'value'];

    public function midlet()
    {
        return $this->belongsTo(MIDlet::class, 'midlet_id');
    }
}
