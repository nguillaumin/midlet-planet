<?php

namespace Tests\Unit\Console\Commands\Import;

use App\Console\Commands\Import\JarManifest;
use PHPUnit\Framework\TestCase;

class JarManifestTest extends TestCase
{
    public function testEmptyString(): void
    {
        $m = new JarManifest('');
        $this->assertEquals([], $m->manifest);
        $this->assertNull($m->getMIDletName());
        $this->assertNull($m->getMIDletVendor());
        $this->assertNull($m->getMIDletVersion());
        $this->assertEquals(0, $m->getMIDletCount());
    }

    public function testManifest(): void
    {
        $m = new JarManifest(
            "Key: value\nKey2: value2\nMIDlet-Name: The name\n" .
                "MIDlet-Vendor: Vendor\nMIDlet-Version: 1.2.3"
        );
        $this->assertEquals([
            'Key'            => ['value'],
            'Key2'           => ['value2'],
            'MIDlet-Name'    => ['The name'],
            'MIDlet-Vendor'  => ['Vendor'],
            'MIDlet-Version' => ['1.2.3'],
        ], $m->manifest);
        $this->assertEquals('The name', $m->getMIDletName());
        $this->assertEquals('Vendor', $m->getMIDletVendor());
        $this->assertEquals('1.2.3', $m->getMIDletVersion());
    }

    public function testMIDlet(): void
    {
        $m = new JarManifest(
            "MIDlet-1: Name1,/icon1.png,Class1\n" .
                "MIDlet-2: Name2,/icon2.png,Class2\n" .
                "MIDlet-3: Name3,/icon3.png\n" .
                "MIDlet-4: Name4\n" .
                'MIDlet-12: Name12'
        );
        $this->assertEquals(5, $m->getMIDletCount());
        $this->assertEquals([
            'name'  => 'Name1',
            'icon'  => '/icon1.png',
            'class' => 'Class1',
        ], $m->getMIDlet(1));
        $this->assertEquals([
            'name'  => 'Name2',
            'icon'  => '/icon2.png',
            'class' => 'Class2',
        ], $m->getMIDlet(2));
        $this->assertEquals([
            'name'  => 'Name3',
            'icon'  => '/icon3.png',
            'class' => '',
        ], $m->getMIDlet(3));
        $this->assertEquals([
            'name'  => 'Name4',
            'icon'  => '',
            'class' => '',
        ], $m->getMIDlet(4));
    }

    public function testDuplicateKey(): void
    {
        $m = new JarManifest("Key: value1\nKey: value2\nOther: other value");
        $this->assertEquals([
            'Key'         => ['value1', 'value2'],
            'Other'       => ['other value'],
        ], $m->manifest);
    }

    public function testUtf8(): void
    {
        $m = new JarManifest(file_get_contents(dirname(__FILE__) . '/JarManifestTest-utf8.txt'));
        $this->assertEquals('俄羅斯方塊', $m->getMIDletName());
    }

    public function testIso8859(): void
    {
        $m = new JarManifest(file_get_contents(dirname(__FILE__) . '/JarManifestTest-iso8859.txt'));
        $this->assertEquals(['Karl H?rnell'], $m->manifest['MIDlet-Vendor']);
    }
}
