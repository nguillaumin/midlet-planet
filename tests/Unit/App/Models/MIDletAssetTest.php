<?php

namespace Tests\Unit\Console\Commands\Import;

use App\Models\MIDletAsset;
use PHPUnit\Framework\TestCase;

class MIDletAssetTest extends TestCase
{
    public function testClusterFolder(): void
    {
        $s = new MIDletAsset();
        $s->ext = 'ext';
        $s->id = 123;
        $this->assertEquals('assets/0/123.ext', $s->getPathAttribute());

        $s->id = 1230;
        $this->assertEquals('assets/0/1230.ext', $s->getPathAttribute());

        $s->id = 12300;
        $this->assertEquals('assets/1/12300.ext', $s->getPathAttribute());

        $s->id = 123000;
        $this->assertEquals('assets/12/123000.ext', $s->getPathAttribute());

        $s->id = 1230000;
        $this->assertEquals('assets/123/1230000.ext', $s->getPathAttribute());
    }
}
