FROM php:8.2-apache

RUN apt-get update && apt-get install -y \
  build-essential \
  libpng-dev \
  libzip-dev \
  libjpeg62-turbo-dev \
  libsqlite3-dev \
  libpq-dev \
  libicu-dev

RUN docker-php-ext-configure gd --with-jpeg
RUN docker-php-ext-install pdo pdo_mysql pdo_pgsql pdo_sqlite gd zip intl

RUN a2enmod rewrite

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

WORKDIR /var/www/html
COPY --chown=www-data:www-data . /var/www/html
