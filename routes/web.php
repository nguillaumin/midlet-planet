<?php

use App\Http\Controllers\MIDletController;
use App\Http\Controllers\MIDletSearchController;
use App\Http\Controllers\StatisticsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::redirect('/', '/midlets');

Route::resource('midlets', MIDletController::class);
Route::get('midlets/{midlet}/title', [MIDletController::class, 'title'])->name('midlets.title');
Route::get('midlets/{midlet}/icon', [MIDletController::class, 'icon'])->name('midlets.icon');
Route::get('midlets/{midlet}/asset/{asset}', [MIDletController::class, 'asset'])->name('midlets.asset');

Route::get('search', [MIDletSearchController::class, 'index'])->name('search.index');
Route::get('search/results', [MIDletSearchController::class, 'search'])->name('search.search');

Route::get('statistics', [StatisticsController::class, 'index'])->name('statistics.index');

Route::get('about', fn () => view('about.index'))->name('about.index');
