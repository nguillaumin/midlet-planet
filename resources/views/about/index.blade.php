@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <p>
                This site attempts to collect and catalog all MIDlets ever released, for
                software preservation purposes.
            </p>

            <p>
                MIDlets were applications and games designed for cell phones around 2000–2010.
                Cell phones at that time had limited capabilities, small displays, keypad input
                and limited network connectivity. For more informations about MIDlets, see the
                <a href="https://en.wikipedia.org/wiki/MIDlet">MIDlet Wikipedia page</a>.
            </p>

            <p>
                The MIDlets on this site were retrieved from different sources, mostly on
                <a href="https://archive.org/">Archive.org</a>:
            </p>

            <ul>
                <li>
                    <a href="https://archive.org/details/1000_J2ME_Games_Pack">
                        [The J2ME Archives] 1000 J2ME Games Pack
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/docomoezweb">
                        DoCoMo i-Mode & EZplus App Archive
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/j2me-archive-unified-2019-09">
                        Unified archive of J2ME software (2019-09)
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/j2me-hipnosis">
                        J2ME Personal Sanitized Archive
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/J2mepacks">
                        Collection of J2ME game packs
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/nokia33103g2017gamepack">
                        Nokia 3310 3G ( 2017) Game Pack
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/Over5000MobileGames">
                        The J2ME Archives ] Over 5000 Mobile Games
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/siemens-e">
                        SIEMENS ( E)
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/240x-320-s-40">
                        240x320 S40 J2ME games
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/800j2megames_201805">
                        [The J2ME Archives] Pack of 800 J2ME games
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/HugeJavaMobileGameDump">
                        Huge Java Mobile Game Dump (67,000 files)
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/nokia-6230-J2ME">
                        Nokia 6230 J2ME games
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/nokia-hry.-jar-cca-560-her">
                        Nokia Games J2ME
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/phoneky_dump">
                        Phoneky/DownloadWAP J2ME/Symbian software downloads scrape
                    </a>
                </li>
                <li>
                    <a href="https://bluemaxima.org/kahvibreak/">
                        Kahvibreak v1.6
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/random-j-2-me">
                        Random J2ME games
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/siemens-x-6-and-x-7">
                        Siemens X6/X7 J2ME games
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/sony-ericsson-k-7-and-k-8">
                        Sony Ericsson K7/K8 J2ME games
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/touch-j-2-me">
                        J2ME Touch games
                    </a>
                </li>
                <li>
                    <a href="https://archive.org/details/siemens-e_202203">
                        SIEMENS (E) [CD 2004]
                    </a>
                </li>
            </ul>

            <p>
                Duplicates have been removed based on a sha256 checksum of the files.
            </p>

            <p>
                This site is build with <a href="https://laravel.com">Laravel</a>,
                <a href="https://getbootstrap.com">Bootstrap</a>,
                <a href="https://icons.getbootstrap.com/">Bootstrap Icons</a>. MIDI player
                from <a href="https://www.midijs.net/">MIDIjs</a>. AMR player from
                <a href="https://github.com/alex374/amr-player">alex374</a>
            </p>
        </div>
    </div>
@endsection
