@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="row g-0 row-cols-2">
                <dl class="col">
                    <dt>MIDlet count</dt>
                    <dd>{{ FormatHelper::number($midletCount) }}
                    <dd>
                </dl>
                <dl class="col">
                    <dt>Total filesize</dt>
                    <dd>{{ FormatHelper::filesize($totalSize) }}
                    <dd>
                </dl>
                <dl class="col">
                    <dt>Title images</dt>
                    <dd>{{ FormatHelper::number($titleCount) }}
                    <dd>
                </dl>
                <dl class="col">
                    <dt>Icon images</dt>
                    <dd>{{ FormatHelper::number($iconCount) }}
                    <dd>
                </dl>
                <dl class="col">
                    <dt>Distinct manifest keys</dt>
                    <dd>{{ FormatHelper::number($distinctManifestKeys) }}</dd>
                </dl>
                <dl class="col">
                    <dt>Asset count</dt>
                    <dd>{{ FormatHelper::number($assetCount) }}</dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <dl>
                <dt>Top manifest attributes</dt>
                <dd>
                    <ul class="row g-0 row-cols-1 row-cols-md-2 list-unstyled">
                        @foreach ($topManifestAttributes as $attr)
                            <li class="col">
                                {{ $attr->key }}
                                <span class="badge bg-secondary-dark">{{ FormatHelper::number($attr->midlets_count) }}</span>
                            </li>
                        @endforeach
                        <ul>
                </dd>
            </dl>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <dl>
                <dt>Top asset mime types</dt>
                <dd>
                    <ul class="row g-0 row-cols-1 row-cols-md-2 list-unstyled">
                        @foreach ($topAssetMimeTypes as $mimeType)
                            <li class="col">
                                {{ $mimeType->mime_type }}
                                <span class="badge bg-secondary-dark">{{ FormatHelper::number($mimeType->asset_count) }}</span>
                            </li>
                        @endforeach
                        <ul>
                </dd>
            </dl>
        </div>
    </div>
    <div class="row g-0 row-cols-2">
        <dl class="col">
            <dt>Biggest MIDlet</dt>
            <dd>
                {{ $biggestMIDlet->name }} by {{ $biggestMIDlet->vendor?->name ?? 'n/a' }}:
                <span class="text-muted">{{ FormatHelper::filesize($biggestMIDlet->filesize) }}</span>
            </dd>
        </dl>
        <dl class="col">
            <dt>Smallest MIDlet</dt>
            <dd>
                {{ $smallestMIDlet->name }} by {{ $smallestMIDlet->vendor?->name ?? 'n/a' }}:
                <span class="text-muted">{{ FormatHelper::filesize($smallestMIDlet->filesize) }}</span>
            </dd>
        </dl>
    </div>
@endsection
