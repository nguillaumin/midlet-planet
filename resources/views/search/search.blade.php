@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <form action="{{ route('search.search') }}" method="get">
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input class="form-control" id="name" name="name" value="{{ old('name') }}">
                </div>
                <div class="mb-3">
                    <label for="filename" class="form-label">Filename</label>
                    <input class="form-control" id="filename" name="filename" value="{{ old('filename') }}">
                </div>
                <div class="mb-3">
                    <label for="vendor" class="form-label @error('vendor') is-invalid @enderror">Vendor</label>
                    <select class="form-select" id="vendor" name="vendor">
                        <option value="" @if (old('vendor') === null) ) selected @endif>-</option>
                        @foreach ($vendors as $vendor)
                            <option value="{{ $vendor->getKey() }}" @if (old('vendor') === $vendor->getKey()) selected @endif>
                                {{ $vendor->name }}</option>
                        @endforeach
                    </select>

                    @error('vendor')
                        <div class="invalid-feedback" role="alert">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="title" class="form-label @error('title') is-invalid @enderror">Has title image</label>
                    <select class="form-select" id="title" name="title">
                        <option value="" @if (old('title') === null) selected @endif>-</option>
                        <option value="true" @if (old('title') === 'true') selected @endif>Yes</option>
                        <option value="false" @if (old('title') === 'false') selected @endif>No</option>
                    </select>

                    @error('title')
                        <div class="invalid-feedback" role="alert">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="icon" class="form-label @error('icon') is-invalid @enderror">Has icon</label>
                    <select class="form-select" id="icon" name="icon">
                        <option value="" @if (old('icon') === null) selected @endif>-</option>
                        <option value="true" @if (old('icon') === 'true') selected @endif>Yes</option>
                        <option value="false" @if (old('icon') === 'false') selected @endif>No</option>
                    </select>

                    @error('icon')
                        <div class="invalid-feedback" role="alert">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="icon" class="form-label @error('midlets') is-invalid @enderror">MIDlet count</label>
                    <select class="form-select" id="midlets" name="midlets">
                        <option value="" @if (old('midlets') === null) selected @endif>-</option>
                        <option value="0" @if (old('midlets') === 0) selected @endif>0</option>
                        <option value="1" @if (old('midlets') === 1) selected @endif>1</option>
                        <option value="2" @if (old('midlets') === 2) selected @endif>2</option>
                        <option value="3" @if (old('midlets') === 3) selected @endif>3 or more</option>
                    </select>
                    <div class="form-text">
                        Each JAR file can contain multiple MIDlets. The vast majority of JAR files contains
                        only one MIDlet.
                    </div>

                    @error('midlets')
                        <div class="invalid-feedback" role="alert">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="icon" class="form-label @error('assetType') is-invalid @enderror">Contain asset of type</label>
                    <select class="form-select" id="assetType" name="assetType">
                        <option value="" @if (old('assetType') === null) selected @endif>-</option>
                        @foreach ($assetTypes as $assetType)
                            <option value="{{ $assetType }}">{{ $assetType }}</option>
                        @endforeach
                    </select>

                    @error('assetType')
                        <div class="invalid-feedback" role="alert">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="screenWidth" class="form-label @error('screenWidth') is-invalid @enderror">Screen width</label>
                        <select class="form-select" id="screenWidth" name="screenWidth">
                            <option value="" @if (old('screenWidth') === null) selected @endif>-</option>
                            @foreach ($screenWidths as $screenWidth)
                                <option value="{{ $screenWidth }}">{{ $screenWidth }}</option>
                            @endforeach
                        </select>

                        @error('screenWidth')
                            <div class="invalid-feedback" role="alert">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="col">
                        <label for="screenHeight" class="form-label @error('screenHeight') is-invalid @enderror">Screen height</label>
                        <select class="form-select" id="screenHeight" name="screenHeight">
                            <option value="" @if (old('screenHeight') === null) selected @endif>-</option>
                            @foreach ($screenHeights as $screenHeight)
                                <option value="{{ $screenHeight }}">{{ $screenHeight }}</option>
                            @endforeach
                        </select>

                        @error('screenHeight')
                            <div class="invalid-feedback" role="alert">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
    </div>
@endsection
