@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>{{ FormatHelper::number($midlets->total()) }} {{ Str::plural('MIDlet', $midlets->total()) }}</h1>
        </div>
    </div>

    <div class="row row-cols-1">
        @foreach ($midlets as $midlet)
            <div class="col">
                @include('midlets.card_midlet')
            </div>
        @endforeach
    </div>
    {{ $midlets->links() }}
@endsection
