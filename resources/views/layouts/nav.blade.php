<nav class="navbar navbar-expand-lg mb-3 bg-secondary-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('midlets.index') }}"><i class="bi bi-globe-americas"></i> MIDlet Planet</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item text-nowrap">
                    <a class="nav-link @if (Request::routeIs('midlets.*')) active @endif" aria-current="page" href="{{ route('midlets.index') }}">
                        <i class="bi bi-house"></i> Home
                    </a>
                </li>
                <li class="nav-item text-nowrap">
                    <a class="nav-link @if (Request::routeIs('search.*')) active @endif" aria-current="page" href="{{ route('search.index') }}">
                        <i class="bi bi-search"></i> Search
                    </a>
                </li>
                <li class="nav-item text-nowrap">
                    <a class="nav-link @if (Request::routeIs('statistics.*')) active @endif" aria-current="page" href="{{ route('statistics.index') }}">
                        <i class="bi bi-graph-up"></i> Statistics
                    </a>
                </li>
                <li class="nav-item text-nowrap">
                    <a class="nav-link @if (Request::routeIs('about.*')) active @endif" aria-current="page" href="{{ route('about.index') }}">
                        <i class="bi bi-question-square"></i> About
                    </a>
                </li>
            </ul>
            <div class="container-fluid text-end">
                <span class="navbar-text">
                    The largest collection of MIDlets on the planet!
                </span>
            </div>
        </div>
    </div>
</nav>
