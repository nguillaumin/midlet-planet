<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', 'MIDlet Planet')</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Oxanium">

    @vite(['resources/sass/app.scss'])
</head>

<body class="bg-secondary">

    <div class="container">
        @include('layouts.nav')

        @yield('content')

        @include('layouts.footer')
    </div>

    @vite(['resources/js/app.js'])

    @yield('scripts')

</body>

</html>
