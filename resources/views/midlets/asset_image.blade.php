<img src="{{ route('midlets.asset', ['midlet' => $midlet, 'asset' => $asset]) }}" class="mb-1 mw-100">
<a href="{{ route('midlets.asset', ['midlet' => $midlet, 'asset' => $asset]) }}">{{ $asset->filename }}</a>
<small class="text-muted">{{ $asset->mime_type }}</small>
