<table class="table table-sm table-striped mt-2 overflow-hidden">
    @foreach ($midlet->manifestAttributes->sortBy('key') as $attribute)
        <tr>
            <th class="fw-normal text-muted text-nowrap">{{ $attribute->key }}</th>
            <td class="overflow-hidden">{{ $attribute->value }}</td>
        </tr>
    @endforeach
</table>
