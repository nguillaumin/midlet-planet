@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            @if ($midlet->icon)
                <img class="img-rendering-pixelated float-end m-1" style="width: 3rem;" alt="Icon for {{ $midlet->name }}"
                    src="{{ route('midlets.icon', $midlet) }}">
            @endif
            <h1>
                {{ $midlet->name }}
                @if ($midlet->version !== null)
                    <span class="text-muted">v{{ $midlet->version }}</span>
                @endif
            </h1>
            @if ($midlet->vendor)
                <h2 class="text-muted">
                    <a
                        href="{{ route('search.search', ['vendor' => $midlet->vendor->getKey()]) }}">{{ $midlet->vendor->name }}</a>
                </h2>
            @endif
            <table class="table table-sm table-borderless">
                <tr>
                    <th>File</th>
                    <td>{{ $midlet->filename }} <span
                            class="text-muted">{{ FormatHelper::filesize($midlet->filesize) }}</span></td>
                </tr>
                <tr>
                    <th>crc32</th>
                    <td><code>{{ $midlet->crc32 }}</code></td>
                </tr>
                <tr>
                    <th>sha1</th>
                    <td><code>{{ $midlet->sha1 }}</code></td>
                </tr>
                <tr>
                    <th>sha256</th>
                    <td><code>{{ $midlet->sha256 }}</code></td>
                </tr>
                @if ($midlet->screen_width && $midlet->screen_height)
                <tr>
                    <th>Resolution</th>
                    <td>{{ $midlet->screen_width }}&times;{{ $midlet->screen_height }}</td>
                </tr>
                @endif
            </table>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-4">
            @if ($midlet->title !== null)
                <img class="img-fluid w-100 img-rendering-pixelated shadow-sm bg-secondary mb-2"
                    alt="Title image for {{ $midlet->name }}" src="{{ route('midlets.title', $midlet) }}">
            @else
                <div class="text-muted text-center my-3">
                    <i class="bi bi-eye-slash fs-1"></i><br>
                    [no title image]
                </div>
            @endif
        </div>
        <div class="col-8">
            @include('midlets.table_manifest')
        </div>
    </div>
    <hr>
    <div class="row row-cols-md-4">
        @foreach ($midlet->assets as $asset)
            <div class="col p-3 d-flex flex-column align-items-center justify-content-between hover-light">
                @if (in_array($asset->main_type, ['image', 'audio']))
                    @include('midlets.asset_' . $asset->main_type)
                @else
                    @include('midlets.asset_default')
                @endif
            </div>
        @endforeach
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="//www.midijs.net/lib/midi.js"></script>
    <script type="text/javascript" src="{{ asset("js/amrnb.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/amrplayer.js") }}"></script>
@endsection
