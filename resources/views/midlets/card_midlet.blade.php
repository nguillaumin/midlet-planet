<div class="card mb-3 bg-secondary">
    <div class="row g-0 p-2 bg-secondary-dark">
        <div class="col">
            <h3 class="fs-5 card-title text-light m-0">
                <span class="text-muted">[{{ $midlet->getKey() }}]</span>
                <a href="{{ route('midlets.show', $midlet) }}">{{ $midlet->name }}</a>
                @if ($midlet->version !== null)
                    <span class="text-muted">v{{ $midlet->version }}</span>
                @endif
                <span class="float-end text-muted">{{ $midlet->vendor?->name }}</span>
            </h3>
        </div>
    </div>
    <div class="row g-0">
        <div class="col-md-4 p-1">
            @if ($midlet->title !== null)
                <img class="img-fluid w-100 img-rendering-pixelated shadow-sm bg-secondary mb-2"
                    alt="Title image for {{ $midlet->name }}"
                    src="{{ route('midlets.title', $midlet) }}">
            @else
                <div class="text-muted text-center my-3">
                    <i class="bi bi-eye-slash fs-1"></i><br>
                    [no title image]
                </div>
            @endif
        </div>
        <div class="col-md-8">
            <div class="card-body p-0">
                @if ($midlet->icon)
                    <img class="img-rendering-pixelated float-end m-1" style="width: 3rem;"
                        alt="Icon for {{ $midlet->name }}"
                        src="{{ route('midlets.icon', $midlet) }}">
                @endif
                @if ($midlet->count !== 1)
                    <div class="px-2">
                        <i class="bi bi-info-circle text-warning"></i>
                        <span class="text-warning">{{ $midlet->count }} {{ Str::plural('MIDlet', $midlet->count) }}</span>
                    </div>
                @endif
                @if ($midlet->screen_width && $midlet->screen_height)
                    <div class="px-2">
                        <i class="bi bi-display"></i> {{ $midlet->screen_width }}&times;{{ $midlet->screen_height }}
                    </div>
                @endif
                @if ($midlet->assets->isNotEmpty())
                    <div class="px-2">
                        <i class="bi bi-file-earmark"></i> {{ $midlet->assets->count() }} assets
                        <ul class="list-inline p-3">
                            @foreach ($midlet->assets as $asset)
                                <li class="list-inline-item">
                                    <i class="bi {{ MIDletHelper::mimeTypeIcon($asset->mime_type) }}"></i>
                                    <a href="{{ route('midlets.asset', ['midlet' => $midlet, 'asset' => $asset]) }}">{{ $asset->filename }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @include('midlets.table_manifest')
            </div>
        </div>
    </div>

    <div class="card-footer">
        <div class="row">
            <div class="col">
                {{ $midlet->filename }}
            </div>
            <div class="col text-center">
                crc32: <code>{{ $midlet->crc32 }}</code>
            </div>
            <div class="col text-end">
                {{ FormatHelper::filesize($midlet->filesize) }}
            </div>
        </div>
    </div>
</div>
