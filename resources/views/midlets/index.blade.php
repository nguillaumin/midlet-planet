@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12 col-md-9 order-md-2">
        <h1>{{ FormatHelper::number($midlets->total()) }} MIDlets</h1>

        @isset($selectedVendor)
            <div class="mb-3">
                <a href="{{ route('midlets.index') }}" class="badge bg-info text-decoration-none">
                    &times; Vendor: {{ $selectedVendor->name }}
                </a>
            </div>
        @endif

        <div class="row row-cols-1">
            @foreach ($midlets as $midlet)
                <div class="col">
                    @include('midlets.card_midlet')
                </div>
            @endforeach
        </div>
        {{ $midlets->links() }}
    </div>

    <div class="col-12 col-md-3 order-md-1">
        <h2>Vendors</h2>
        <ul class="list-unstyled">
            @foreach ($vendors as $vendor)
                @if ($loop->index == 10)
                    <li>
                        <ul class="list-unstyled collapse" id="collapse-vendors">
                @endif
                <li>
                    <a href="{{ route('midlets.index', ['vendor' => $vendor->id]) }}">{{ $vendor->name }}</a>
                    <span class="badge bg-secondary-dark">{{ $vendor->midlets_count }}</span>
                </li>
            @endforeach
            @if ($vendors->count() > 9)
                    </ul>
                </li>
                <a href="#" data-bs-toggle="collapse" data-bs-target="#collapse-vendors">more…</a>
            @endif
        </ul>
    </div>

</div>
@endsection
