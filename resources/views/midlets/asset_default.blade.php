<i class="text-muted fs-1 bi {{ MIDletHelper::mimeTypeIcon($asset->mime_type) }}"></i><br>
<a href="{{ route('midlets.asset', ['midlet' => $midlet, 'asset' => $asset]) }}">{{ $asset->filename }}</a>
<small class="text-muted">{{ $asset->mime_type }}</small>
