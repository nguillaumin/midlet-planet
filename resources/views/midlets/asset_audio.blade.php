@switch ($asset->subtype)
    @case('midi')
    @case('amr')
        <a href="javascript:;"
            data-mp-{{ $asset->subtype }}="{{ route('midlets.asset', ['midlet' => $midlet, 'asset' => $asset]) }}">
            <i class="fs-1 bi bi-play-circle"></i>
        </a>
        <a href="{{ route('midlets.asset', ['midlet' => $midlet, 'asset' => $asset]) }}">{{ $asset->filename }}</a>
        <small class="text-muted">{{ $asset->mime_type }}</small>
        @break

    @default
            @include('midlets.asset_default')
    @endswitch
