/**
 * Handle playing of AMR assets
 */
document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('[data-mp-amr]').forEach((el) => {
        // React to an event to stop playing, if another AMR asset is
        // being played
        el.addEventListener('mp.amr.stop', () => {
            if (el.mpAmrPlayer) {
                if (el.mpAmrPlayer.isPlaying) {
                    el.mpAmrPlayer.pause();
                }
                el.mpAmrPlayer = null;
            }

            el.dataset.mpAmrPlaying = false;

            el.querySelectorAll('i.bi').forEach((i) => {
                i.classList.remove('bi-stop-circle');
                i.classList.add('bi-play-circle');
            });
        });

        // React to click event to start/stop playing
        el.addEventListener('click', () => {
            var player = el.mpAmrPlayer;

            // Play state is stored in the element dataset
            var playing = el.dataset.mpAmrPlaying === 'true' || false;

            if (!playing) {
                player = new window.AmrPlayer(el.dataset.mpAmr, () => {
                    player.endedWith(() => {
                        el.dispatchEvent(new CustomEvent('mp.amr.stop'));
                    });
                    setTimeout(() => {
                        player.play();
                    }, 1);
                });
                el.mpAmrPlayer = player;
            } else if (player) {
                player.pause();
            }

            playing = !playing;

            // Update state and icon
            el.dataset.mpAmrPlaying = playing;
            el.querySelectorAll('i.bi').forEach((i) => {
                if (playing) {
                    i.classList.remove('bi-play-circle');
                    i.classList.add('bi-stop-circle');
                } else {
                    i.classList.remove('bi-stop-circle');
                    i.classList.add('bi-play-circle');
                }
            });

            // Dispatch an event to update the icon of all other
            // AMR assets to the stop icon
            var evt = new CustomEvent('mp.amr.stop');
            document.querySelectorAll('[data-mp-amr]').forEach((amr) => {
                if (amr !== el) {
                    amr.dispatchEvent(evt);
                }
            });
        });
    });
});
