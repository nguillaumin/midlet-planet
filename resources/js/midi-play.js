/**
 * Handle playing of MIDI assets
 */
document.addEventListener('DOMContentLoaded', () => {
    if (window.MIDIjs) {
        window.MIDIjs.player_callback = (e) => { // eslint-disable-line camelcase
            if (e.status === 'finished') {
                document.querySelectorAll('[data-mp-midi]').forEach((midi) => {
                    midi.dispatchEvent(new CustomEvent('mp.midi.stop'));
                });
            }
        };
    }

    document.querySelectorAll('[data-mp-midi]').forEach((el) => {

        // React to an event to stop playing, if another MIDI asset is
        // being played
        el.addEventListener('mp.midi.stop', () => {
            el.dataset.mpMidiPlaying = false;

            el.querySelectorAll('i.bi').forEach((i) => {
                i.classList.remove('bi-stop-circle');
                i.classList.add('bi-play-circle');
            });
        });

        // React to click event to start/stop playing
        el.addEventListener('click', () => {

            // Play state is stored in the element dataset
            var playing = el.dataset.mpMidiPlaying === 'true' || false;

            if (!playing) {
                window.MIDIjs.play(el.dataset.mpMidi);
            } else {
                window.MIDIjs.stop();
            }

            playing = !playing;

            // Update state and icon
            el.dataset.mpMidiPlaying = playing;
            el.querySelectorAll('i.bi').forEach((i) => {
                if (playing) {
                    i.classList.remove('bi-play-circle');
                    i.classList.add('bi-stop-circle');
                } else {
                    i.classList.remove('bi-stop-circle');
                    i.classList.add('bi-play-circle');
                }
            });

            // Dispatch an event to update the icon of all other
            // MIDI assets to the stop icon
            var evt = new CustomEvent('mp.midi.stop');
            document.querySelectorAll('[data-mp-midi]').forEach((midi) => {
                if (midi !== el) {
                    midi.dispatchEvent(evt);
                }
            });
        });
    });
});
