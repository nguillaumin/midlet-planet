<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('midlets', function (Blueprint $table) {
            $table->id();
            $table->string('name', 1024)->index();
            $table->string('filename', 1024)->index();
            $table->integer('filesize');
            $table->integer('count');
            $table->string('version', 64)->nullable();
            $table->integer('screen_width')->nullable();
            $table->integer('screen_height')->nullable();

            // SHA-256 should be unique
            $table->string('sha256', 64)->unique();

            // SHA-1 and CRC32 might not be unique (higher risk
            // of collisions)
            $table->string('sha1', 40)->index();
            $table->string('crc32', 8)->index();
            $table->timestamps();
        });

        Schema::create('midlet_manifest_attributes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('midlet_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->string('key')->index();
            $table->text('value');
            $table->timestamps();
        });

        Schema::create('midlet_assets', function (Blueprint $table) {
            $table->id();
            $table->string('filename', 255);
            $table->string('ext', 64);
            $table->string('mime_type', 255)->index();
            $table->enum('type', ['title', 'icon', 'asset'])->nullable()->index();
            $table->timestamps();

            $table->foreignId('midlet_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::drop('midlets');
        Schema::drop('midlet_manifest_attributes');
        Schema::drop('midlet_assets');
    }
};
